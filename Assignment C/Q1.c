#include <stdio.h>
int totIncome(int);
int totCost(int);
int profit(int);
int noOfAttendees(int);

int noOfAttendees(int ticketPrice){

return 120 - (ticketPrice-15)/5*20;

}

int totIncome(int ticketPrice){

return ticketPrice * noOfAttendees(ticketPrice);

}

int totCost(int ticketPrice){

return 500 + 3 * noOfAttendees(ticketPrice);

}

int profit(int ticketPrice){

return totIncome(ticketPrice) - totCost(ticketPrice);

}


int main (){


int ticketPrice;

printf("Relationship between profit and ticket price \n\n");

printf("Ticket Price(Rs.)\t\t profit(Rs.)\n\n");

for (ticketPrice=0;ticketPrice<50;ticketPrice+=5){

    printf("\t%d.00\t\t\t%d.00\n",ticketPrice,profit(ticketPrice));


}

return 0;

}
